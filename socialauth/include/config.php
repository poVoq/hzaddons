<?php

// wrapper for configuration mgmt
class SocialAuthConfig {

	// prevents instantiation
	private function __construct() {}

	// cache config to save DB requests
	static private $synced = false;
	static private $config;

	static private $supported_providers = [
		'Google',
		'Facebook',
		'Twitter',
	];

	static private function createPrefix($provider) {
		return $provider.'_';
	}

	static public function getKey($provider, $key) {
		return self::createPrefix($provider).$key;
	}

	static public function getSupportedProviders() {
		return self::$supported_providers;
	}

	static public function getConfig() {

		if (self::$synced) 
			return self::$config;
	
		// TODO limit scope to email only

		// TODO build this config dynamically based on SocialAuth App configuration
		$hybridauthconfig = [
			'callback' => z_root() . '/socialauthsignin.php',
			'providers' => [],
		];

		foreach(self::getSupportedProviders() as $provider) {

			$enabled = get_config('socialauth', self::getKey($provider,'enabled')) == 1 ? true : false;
			$key = get_config('socialauth', self::getKey($provider,'key'));
			$secret = get_config('socialauth', self::getKey($provider,'secret'));

			$r = [
				$provider => [
					'enabled' => $enabled,
					'keys' => [
						'key' => $key,
						'secret' => $secret,
					],
					'scope' => 'https://www.googleapis.com/auth/userinfo.email',
				],
			];

			$hybridauthconfig['providers'] = array_merge($hybridauthconfig['providers'], $r);
		}

		self::$config = $hybridauthconfig;
		self::$synced = true;

		return $hybridauthconfig;
	}

	// try to save a configuration field related to socialauth app
	// If it's not a social auth app configuration field, it fails silently
	static public function save($field, $value) {

		foreach(self::getSupportedProviders() as $provider) {
			$keys = ['enabled', 'key', 'secret'];
			foreach($keys as $key) {
				if ( $field === self::getKey($provider, $key) ) {
					logger('Setting config for key '. $field, LOGGER_DEBUG);
					set_config('socialauth', $field, $value);
					self::$synced = false;
				}
			}

		}
	}
 
}
