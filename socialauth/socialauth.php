<?php

/**
 * Name: Social auth 
 * Description: Login to Hubzilla using a social account (Google, Facebook, Twitter etc) 
 * Version: 0.0
 * Author: Pascal Deklerck <http://hub.eenoog.org/profile/pascal>
 * Maintainer: Pascal Deklerck <pascal.deklerck@gmail.com> 
 */


use Zotlabs\Extend\Hook;
use Zotlabs\Extend\Route;


function socialauth_load() {

	logger('Socialauth load', LOGGER_DEBUG);

	Hook::register('login_hook', 'addon/socialauth/socialauth.php', 'socialauth_login');

	Route::register('addon/socialauth/Mod_SocialAuth.php','socialauthsignin');
	Route::register('addon/socialauth/Mod_SocialAuth.php','socialauth');
}


function socialauth_unload() {

	logger('Socialauth unload', LOGGER_DEBUG);
	Hook::unregister('login_hook', 'addon/socialauth/socialauth.php', 'socialauth_login');

	Route::unregister('addon/socialauth/Mod_SocialAuth.php','socialauthsignin');
	Route::unregister('addon/socialauth/Mod_SocialAuth.php','socialauth');
}

function socialauth_login(&$o) {

	logger('callback socialauth_login called', LOGGER_DEBUG);

	require_once __DIR__ . '/include/config.php';
	$config = SocialAuthConfig::getConfig();
	
	$adapter_output = "";
	
	foreach($config['providers'] as $name => $provider) {
		if ( $provider['enabled'] ) {
			$buttonFileName = __DIR__ . '/buttons/button_'. $name .'.html';
			if ( file_exists ( $buttonFileName ) )
			{
				$redirect_url = $config['callback'] . '?provider=' . $name;
				$adapter_output .= str_replace('socialauth_redirect', 'redirect_socialauth(\''.$redirect_url.'\');', file_get_contents($buttonFileName) );

			} else {
				logger('Button file missing - skipping provider '.$name, LOGGER_NORMAL, LOG_WARNING);
				$adapter_output .= ' 
					<ul>
	<li>
		<a href="'. $config["callback"] . '?provider='. $name .'"/>Sign in with <strong>'. $name .'</strong>
	</li>
</ul>
';
			}
		}
	}

//	logger('adapter output: '. print_r($adapter_output, true), LOGGER_DEBUG);

	$o .= '
<head>
<script>
function redirect_socialauth(url) {
	location.href = url;
}
</script>
<style>
.socialauthlabel {
	padding-top: 2em;
	font-weight: bold;
	display: block;
	margin-bottom: .5rem;
	max-width: 400px;
	margin: auto;
}
</style>
</head>
<body>
<div class="socialauthlabel">
<span>Sign in with:</span>
</div>
</body>
'. $adapter_output;
 
	return $o;
}

