<?php


namespace Zotlabs\Module;

use App;
use Zotlabs\Lib\Apps;
use Zotlabs\Web\Controller;

// Sign in logic
class SocialAuthSignin extends Controller {

	function get() {
		logger('SocialAuth Signin controller GET', LOGGER_DEBUG);

		require_once __DIR__ . '/include/config.php';
		$config = \SocialAuthConfig::getConfig();

		$provider = $_GET['provider'];
		if (x($provider)) {

			logger('Request provider = '. $provider, LOGGER_DEBUG);
			
			// Check if provider is supported
			if (!array_key_exists($provider, $config['providers'])) {
				logger('Provider "'. $provider . '" not supported - ABORT', LOGGER_DEBUG);
				goaway(z_root());
			}

		} else {
			logger('No provider specified', LOGGER_DEBUG);
		}

		require __DIR__ . '/vendor/autoload.php';
		$auth = new \Hybridauth\Hybridauth($config);

		// second pass: check if user is connected to a provider
		if (!x($provider)) {
			$storage = new \Hybridauth\Storage\Session();
			$provider = $storage->get('provider');

			if (!x($provider)) {
				logger('Socialauth - Provider not detected', LOGGER_DEBUG);
				goaway(z_root());
			}

			$adapter = $auth->authenticate($provider);

			if ($adapter->isConnected()) {
				logger('Socialauth - Connected to '. $provider .' OK', LOGGER_DEBUG);

				// clear session provider parameter
				$storage->set('provider', null);

				socialauth_signin($adapter);
			} else {
				logger('Socialauth - Authentication failed with provider '. $provider, LOGGER_DEBUG);
				goaway(z_root());
			}

		}

		// first pass: provider is ... provided
		if ($auth->isConnectedWith($provider)) {
			logger('Socialauth - connected to ' . $provider. '!', LOGGER_DEBUG);
			
			$adapter = $auth->getAdapter($provider);

			socialauth_signin($adapter);
		} else {
			logger('Socialauth - not connected to ' . $provider . ' yet', LOGGER_DEBUG);

			// remember provider for callback
			$storage = new \Hybridauth\Storage\Session();
			$storage->set('provider', $provider); 
			
			// authentication should trigger callback from the provider to this page
			$adapter = $auth->authenticate($provider);
		}

	}
}

function socialauth_signin($adapter)
{
	if (!x($adapter) || !($adapter->isConnected()))
	{
		logger('Invalid adapter', LOGGER_NORMAL, LOG_ERR);
		goaway(z_root());
	}

	logger('Retrieved user profile: '. print_r($adapter->getUserProfile(), true), LOGGER_DEBUG);
	$email = $adapter->getUserProfile()->email;
	if (!x($email)) {
		logger('Cannot retrieve email address', LOGGER_NORMAL, LOG_ERR);
		goaway(z_root());
	}

	// all ok, continue as logged in user
	logger('Logging in with email ' . $email, LOGGER_DEBUG);

	$r = q("select * from account where account_email = '%s' LIMIT 1", $email);
	if (count($r)) {
		$record = $r[0];

		logger('Account: ' . print_r($record, true), LOGGER_TRACE);

		$channel_id = $record['account_default_channel'];
	        $_SESSION['uid'] = $channel_id;
			
		require_once('include/security.php');
		authenticate_success($record, null, true, false, true, true);

	} else {
		logger('Email address '. $email .' not found in database', LOGGER_DEBUG);
		info('Unable to login using email address '. $email);
	}

	$adapter->disconnect();
	goaway(z_root());

}


// Social Auth App configuration
class SocialAuth extends Controller {

	function get() {
		if(! local_channel())
			return;

		if(! Apps::addon_app_installed(local_channel(), 'socialauth')) {
			//Do not display any associated widgets at this point
			App::$pdl = '';

			$o = '<b>Social Authentication App (Not Installed):</b><br>';
			$o .= t('Sign in to Hubzilla using your social account');
			return $o;
		}

		$content .= '<div class="section-content-info-wrapper">';
		$content .= t('Social Authentication using your social media account');
		$content .= '</div>';

		$content .= '<div class="section-content-info-wrapper">';
		$content .= t('This app enables one or more social provider sign-in buttons on the login page.');
                $content .= '</div>';
		
		$yes_no = array(t('No'),t('Yes'));

		require_once __DIR__ . '/include/config.php';
		$config = \SocialAuthConfig::getConfig();

		foreach($config['providers'] as $name => $provider) {
			$enabled = $provider['enabled'] ? 1 : 0;
			$key = $provider['keys']['key'];
			$secret = $provider['keys']['secret'];

			// radio button enabled/disabled
			$content .= replace_macros(get_markup_template('field_checkbox.tpl'),
				[	
					'$field' => [\SocialAuthConfig::getKey($name, 'enabled'), t('Enable provider ').$name, $enabled, '', $yes_no]
				]
			);

			// let the user configure key & secret
			$content .= replace_macros(get_markup_template('field_input.tpl'), 
				[
					'$field' => [\SocialAuthConfig::getKey($name, 'key'), t('Key'), $key, t('Word')]
				]
			);
			$content .= replace_macros(get_markup_template('field_input.tpl'), 
				[
					'$field' => [\SocialAuthConfig::getKey($name, 'secret'), t('Secret'), $secret, t('Word')]
				]
			);
		}
 
		$tpl = get_markup_template("settings_addon.tpl");

		$o = replace_macros($tpl, array(
			'$action_url' => 'socialauth',
			'$form_security_token' => get_form_security_token("socialauth"),
			'$title' => t('Social authentication'),
			'$content'  => $content,
			'$baseurl'   => z_root(),
			'$submit'    => t('Submit'),
		));
		
		return $o;
	}
	
	function post() {

		if(! local_channel())
			return;

		if(! Apps::addon_app_installed(local_channel(),'socialauth'))
			return;

		check_form_security_token_redirectOnErr('socialauth', 'socialauth');

		require_once __DIR__ . '/include/config.php';
		// if 'enabled' is set to 'No', then the 'enabled' parameter is not present in POST array => disable by default
		foreach (\SocialAuthConfig::getSupportedProviders() as $name)
		{
			$key = \SocialAuthConfig::getKey($name, 'enabled');
			$enabled = x($_POST, $key) ? 1 : 0;
			\SocialAuthConfig::save($key, $enabled);
		}

		foreach ($_POST as $param => $value) {
			\SocialAuthConfig::save($param, $value);
		}

		info( t('Social authentication settings saved.') . EOL);
	}
}
 
