<?php

/**
 * Name: Google Sign in 
 * Description: Login to Hubzilla using your Google account 
 * Version: 0.0
 * Author: Pascal Deklerck <http://hub.eenoog.org/profile/pascal>
 * Maintainer: Pascal Deklerck <pascal.deklerck@gmail.com> 
 */


//use Zotlabs\Lib\Apps;
use Zotlabs\Extend\Hook;
use Zotlabs\Extend\Route;

function googlesignin_load() {
	Hook::register('login_hook', 'addon/googlesignin/googlesignin.php', 'googlelogin');
	Hook::register('content_security_policy', 'addon/googlesignin/googlesignin.php', 'google_set_cspsetting');

	Route::register('addon/googlesignin/Mod_GoogleSignIn.php','googletokensignin');
	Route::register('addon/googlesignin/Mod_GoogleSignIn.php','googlesignin');
}


function googlesignin_unload() {
	Hook::unregister('login_hook', 'addon/googlesignin/googlesignin.php', 'googlelogin');
	Hook::unregister('content_security_policy', 'addon/googlesignin/googlesignin.php', 'google_set_cspsetting');

	Route::unregister('addon/googlesignin/Mod_GoogleSignIn.php','googletokensignin');
	Route::unregister('addon/googlesignin/Mod_GoogleSignIn.php','googlesignin');
}

function google_set_cspsetting(&$cspsettings)
{
	// must be loaded from CDN due to Google terms
	// see https://stackoverflow.com/questions/36429805/how-to-include-google-maps-js-library-locally-and-not-from-cdn
	$cspsettings['script-src'][]="apis.google.com";
}

function googlelogin(&$o) {

	logger('Google Login callback called', LOGGER_DEBUG);

	$clientID = get_config('googlesignin', 'clientID');

	// TODO implement greyed-out sign-in button when ClientID is empty or invalid

	$o .= '
<head>
	<meta name="google-signin-scope" content="email">
	<meta name="google-signin-client_id" content="'. $clientID . '">
	<script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
</head>
<body>
<script>
	function init() {
		gapi.load("auth2", function() {
			gapi.auth2.init().then(function() {
				var auth2 = gapi.auth2.getAuthInstance();
				if (auth2.isSignedIn.get()) { 
					console.log("Info: User is signed in") 
				}
				else { 
					console.log("Info: User is NOT signed in"); 
				}
				auth2.attachClickHandler(document.getElementById("customBtn"),{},onSignIn,function(error){});			
			});
		});
	}

	function onSignIn(googleUser) {
	        // The ID token you need to pass to your backend:
	        var id_token = googleUser.getAuthResponse().id_token;

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/googletokensignin");
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.onload = function() {
			var result = JSON.parse(xhr.responseText);
			if (result.error_message) {
				alert(result.error_message);
			} else {
				console.log("Signed in as: " + result.account_email);
		
				// redirect
				console.log("Redirecting to: " + result.URL);
				window.location.href = result.URL;
			}
		};
		xhr.send("idtoken=" + id_token);
	}
</script>
</body>

<head>
<style type="text/css">
#customBtn {
	display: inline-block;
	background: white;
	color: #444;
	width: 100%;
        border: thin solid #888;
	white-space: nowrap; */
}
#customBtn:hover {
        cursor: pointer;
        box-shadow: 1px 1px 1px grey;
}
span.label {
	font-weight: bold;
	display: inline-block;
	margin-bottom: .5rem;
}
span.buttonText {
	display: inline-block;
	vertical-align: middle;
	padding-left: 42px;
	padding-right: 42px;
	font-size: 14px;
	font-weight: bold;
	font-family: "Roboto", sans-serif;
}
.addButton {
	padding-top: 2em;
	display: block;
	max-width: 400px;
	margin: auto;
}	
span.abcRioButtonIcon {
  float: left
}
</style>								    
</head>
<body>
<div id="gSignInWrapper" class="addButton">
     <span class="label">Sign in with:</span>  
     <div id="customBtn" class="btn customGPlusSignIn" align=center>
     <span class="abcRioButtonIcon">
        <div style="width:18px;height:18px;" class="abcRioButtonSvgImageWithFallback abcRioButtonIconImage abcRioButtonIconImage18">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" viewBox="0 0 48 48" class="abcRioButtonSvg">
            <g>
              <path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path>
              <path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path>
              <path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path>
              <path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path>
              <path fill="none" d="M0 0h48v48H0z"></path>
            </g>
          </svg>
        </div>
      </span>
      <span class="buttonText">Google</span>
    </div>
  </div>
  <div id="name"></div>
</body>

';
 
	return $o;
}

