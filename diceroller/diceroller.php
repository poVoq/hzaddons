<?php

/**
 * Name: Dice roller 
 * Description: Roll dice based on specific mark-up in posts 
 * Version: 0.3 
 * Author: Pascal Deklerck <http://hub.eenoog.org/profile/pascal>
 * Maintainer: 
 */


use Zotlabs\Extend\Hook;

require_once('DiceGame.php');

function diceroller_install() {
	Hook::register('post_local', 'addon/diceroller/diceroller.php', 'diceroller_post_local', 10);
	Hook::register('display_item', 'addon/diceroller/diceroller.php', 'diceroller_display_item', 10);
}


function diceroller_uninstall() {
	Hook::unregister('post_local', 'addon/diceroller/diceroller.php', 'diceroller_post_local');
	Hook::unregister('display_item', 'addon/diceroller/diceroller.php', 'diceroller_display_item', 10);
}

// single function to detect if a text matches a dice roll
function diceroller_detect_roll($text) {
	$result = array( 'match' => false );
	
	// regular expression defines widest range of features we support
	// typical it must start by '%roll AdX' meaning "roll A dices, X = nb of faces or any other valid kind of roll"
	$diceroller_match_regex = '/^%roll (\d*)d(\d+|F)/';

	if ( preg_match($diceroller_match_regex, $text, $matches) ) {
		logger('Regex match : 1 = <' . $matches[1] . '> 2 = <' . $matches[2] . '>', LOGGER_DEBUG);
		$result['match'] = true;
		$result['dice'] = ( $matches[1] === '' ? 1 : $matches[1] );
		$result['faces'] = $matches[2];
	}
	else {
		logger('No regex match', LOGGER_DEBUG);
		
		// the shortcut '%roll' means: 1 dice, 6 faces
		if ( $text === "%roll" ) {
			logger('Default roll match', LOGGER_DEBUG);
			$result['match'] = true;
			$result['dice'] = 1;
			$result['faces'] = 6;
		}
	}

	return $result;
}

// look for the specific dice rolling mark-up: %roll <dice> where <dice> is the general notation for dice rolling (https://en.wikipedia.org/wiki/Dice_notation)
function diceroller_post_local(&$item) {

	$body = $item['body'];

	logger('Text to evaluate: <' . print_r($body, true) . '>', LOGGER_DATA);

	$match_result = diceroller_detect_roll($body);
	if ( $match_result['match'] ) {
		logger('Detected valid dice roll request : ' . $body, LOGGER_DEBUG);

		$dice = $match_result['dice'];
		$faces = $match_result['faces'];

		$result = 0;
		$strres = '';

		// protect against too large output
		$maxdice = 10;
		if ( $dice > $maxdice ) {
			notice( t('Too many dice. Maximum '.$maxdice ) . EOL);
			$item['cancel'] = true;
			return;
		}

		logger('Settling for ' . $dice . ' dice, "faces" = '. $faces,  LOGGER_DEBUG);

		// create game interpretation object
		if ( $faces === 'F' ) {
			$diceGame = new FudgeDiceGame($dice);
		}
		else {
			$diceGame = new DefaultDiceGame($dice, $faces);
		}

		logger('Dice game created', LOGGER_DEBUG);
		$diceGame->roll();
		logger('Dice rolling done', LOGGER_DEBUG);

		$result = $diceGame->getResult();
		$output = $diceGame->getOutput();

		$strres = $output . ( ( empty($result) && ( $result !== "0") ) ? '' : ' (= ' . $result . ')' );

		$item['title'] = 'Dice roll result = ' . $strres;

		// add current time to make this post unique - else it might get picked up as a duplicate
		$curtime = date(DATE_RFC2822);
		$item['body'] .= ' - Result: ' . $strres . '
Rolled on '. $curtime;
	}
}

// disable editing or deleting of (own) post - no cheating!
function diceroller_display_item(&$arr) {

	$body = $arr['item']['body'];

	$match_result = diceroller_detect_roll($body);
	if ( $match_result['match'] ) {
		logger('Disabling editing & deleting for this item', LOGGER_DEBUG);
		$arr['output']['edpost'] = false;
		$arr['output']['drop'] = false;
	}
}
