<?php

require_once('TestBoot.php');
require_once('../diceroller.php');

function logger($text, $level) {
	print log_level_str($level) ." : " . $text . "\n";
}

function test_input($input) {
	$match_result = diceroller_detect_roll($input);
	
	if ( $match_result['match'] ) {
		print "input = " . $input . " - Match OK\n";
		print "  \"dice\" : " . $match_result['dice'] . "\n";
		print "  \"faces\" : " . $match_result['faces'] . "\n";
	}
	else {
		print "input = " . $input . " - No match - FAIL!\n";
	}
}

print "Testing DiceGame\n";

print "1. Regular expression matching\n";

test_input('%roll 3d6');
test_input('%roll 10d6');
test_input('%roll 4dF');
test_input('%roll d8');
test_input('%roll dF');
test_input('%roll');

print "2. Different game output per type of dice game\n";

$defaultDiceGame = new DefaultDiceGame(3,6);  // 3 dice, 6 faces each
$defaultDiceGame->roll();
print "Default Dice game TEST: output = <" . $defaultDiceGame->getOutput() . "> - Result = <" . $defaultDiceGame->getResult() . ">\n";

$fudgeDiceGame = new FudgeDiceGame(4); // 4 dice fudge game
$fudgeDiceGame->roll();
print "Fudge Dice game TEST: output = <" . $fudgeDiceGame->getOutput() . "> - Result = <" . $fudgeDiceGame->getResult() . ">\n";

?>
